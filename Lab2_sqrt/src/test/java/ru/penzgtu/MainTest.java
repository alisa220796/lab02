package ru.penzgtu;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @org.junit.jupiter.api.Test
    void sqrtTrue() {
        assertEquals(2, Main.sqrt(4));
    }
    @Test
    void sqrtFalse() {
        assertNotEquals(1, Main.sqrt(4));
    }

}